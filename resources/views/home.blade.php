@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        @include('pages.home.monthly')

        <div class="card o-income">
            <div class="card-body">
                <h5 class="mb-3">Chart</h5>
                <div class="d-flex m-b-30 no-block">
                    <h5 class="card-title m-b-0 align-self-center chart-type">
                        <a data-value="order_monthly" class="btn btn-primary btn-report" href="#" role="button">Order Bulan Ini</a>
                        <a data-value="order_yearly" class="btn btn-primary btn-report" href="#" role="button">Order Tahun Ini</a>
                        <a data-value="booking_monthly" class="btn btn-primary btn-report" href="#" role="button">Booking Bulan Ini</a>
                        <a data-value="booking_yearly" class="btn btn-primary btn-report" href="#" role="button">Booking Tahun Ini</a>
                    </h5>

                    <div class="ml-auto" id="color-info"></div>
                </div>

                <div id="line-report"></div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset('assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
@endpush
@push('js')
    <script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/libs/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(function() {
            var start = moment().subtract(1, 'months');
            var end = moment();

            $(".btn-report").click(function() {
                if ($(this).hasClass('btn-primary')) {
                    $(".btn-report").removeClass('btn-secondary').addClass('btn-primary');
                    $(this).removeClass('btn-primary').addClass('btn-secondary');
                    setBar();
                }
            });
            $(".chart-type a:first").trigger('click');
        });

        function setBar() {
            $("#line-report").empty();

            var active = $(".btn-report.btn-secondary").data('value');
            var chart = Morris.Line({
                element: 'line-report',
                xkey: 'date',
                ykeys: [active],
                labels: [active],
                xLabelAngle: 60,
                xLabelMargin: 10,
                lineColors: ['#7460ee'],
            });

            $.getJSON("{{ route('home.report') }}", {
                    type: active,
                },
                function(data, textStatus, jqXHR) {
                    chart.setData(data);
                    $("#color-info").html('<ul class="list-inline text-right">' +
                        '<li class="list-inline-item">' +
                        '<h5><i class="fa fa-circle m-r-5 text-primary"></i>' + active + '</h5>' +
                        '</li>' +
                        '</ul>');
                }
            );
        }
    </script>
@endpush
