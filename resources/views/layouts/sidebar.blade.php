<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">
                    <span class="hide-menu">Saldo<br>
                        <h5>Rp. {{ number_format($balance) }}</h5>
                    </span>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false">
                        <i class="icon-Car-Wheel"></i>
                        <span class="hide-menu">Dashboard </span>
                    </a>
                </li>

                @can('user.view', 'permission.view', 'role.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false">
                            <i class="mdi mdi-account-multiple"></i>
                            <span class="hide-menu">User </span>
                        </a>
                        <ul aria-expanded="false" class="collapse first-level">
                            @can('agent.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('agent.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu"> Agent List </span>
                                    </a>
                                </li>
                            @endcan

                            @can('user.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('user.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu"> User List </span>
                                    </a>
                                </li>
                            @endcan

                            @can('permission.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('permission.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-adjust"></i><span class="hide-menu">User Permission</span>
                                    </a>
                                </li>
                            @endcan

                            @can('role.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('role.index') }}" class="sidebar-link"><i
                                            class="mdi mdi-adjust"></i><span class="hide-menu">User Role</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('balance.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false"><i class="mdi mdi-cash-multiple"></i><span
                                class="hide-menu">Keuangan</span></a>

                        <ul aria-expanded="false" class="collapse first-level">
                            @can('balance.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('balance.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-cash-multiple"></i>
                                        <span class="hide-menu"> Riwayat Saldo</span>
                                    </a>
                                </li>
                            @endcan

                            @can('balance.unpaid')
                                <li class="sidebar-item">
                                    <a href="{{ route('balance.index', ['type' => 'waiting-confirmation']) }}" class="sidebar-link">
                                        <i class="mdi mdi-cash-multiple"></i>
                                        <span class="hide-menu"> Menunggu Konfirmasi</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('deposit.schedule')
                    <li class="sidebar-item">
                        <a href="{{ route('deposit-schedule.index') }}" class="sidebar-link">
                            <i class="mdi mdi-av-timer"></i>
                            <span class="hide-menu"> Deposit Schedule </span>
                        </a>
                    </li>
                @endcan

                @can('category.view')
                    <li class="sidebar-item">
                        <a href="{{ route('category.index') }}" class="sidebar-link">
                            <i class="mdi mdi-view-list"></i>
                            <span class="hide-menu"> Category </span>
                        </a>
                    </li>
                @endcan

                @can('product.view')
                    <li class="sidebar-item">
                        <a href="{{ route('product.index') }}" class="sidebar-link">
                            <i class="mdi mdi-cube"></i>
                            <span class="hide-menu"> Product </span>
                        </a>
                    </li>
                @endcan

                @can('order-pra-bayar.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false"><i class="mdi mdi-cart-outline"></i><span
                                class="hide-menu">Order</span></a>

                        <ul aria-expanded="false" class="collapse first-level">
                            <li class="sidebar-item">
                                <a href="{{ route('order-pra-bayar.index') }}" class="sidebar-link">
                                    <i class="mdi mdi-cash-multiple"></i>
                                    <span class="hide-menu"> Pra Bayar</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{ route('order-pasca-bayar.index') }}" class="sidebar-link">
                                    <i class="mdi mdi-cash-multiple"></i>
                                    <span class="hide-menu"> Pasca Bayar</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{ route('order-product.index') }}" class="sidebar-link">
                                    <i class="mdi mdi-cash-multiple"></i>
                                    <span class="hide-menu"> Product</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('booking.view')
                    <li class="sidebar-item">
                        <a href="{{ route('booking.index') }}" class="sidebar-link">
                            <i class="mdi mdi-checkbox-multiple-marked-outline"></i>
                            <span class="hide-menu"> Booking </span>
                        </a>
                    </li>
                @endcan

            </ul>
        </nav>
    </div>
</aside>
