<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="d-md-flex align-items-center">
                    <div>
                        <h4 class="card-title">Statistics</h4>
                        <h5 class="card-subtitle">Overview of Statistics</h5>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table v-middle">
                    <thead>
                        <tr class="bg-light">
                            <th class="border-top-0">Name</th>
                            <th class="border-top-0">Today</th>
                            <th class="border-top-0">Monthly</th>
                            <th class="border-top-0">Annual</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($statistics as $data)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="m-r-10">
                                            <a class="btn btn-circle text-white {{ data_get($data, 'color') }}">
                                                {{ data_get($data, 'short') }}
                                            </a>
                                        </div>
                                        <div class="">
                                            <h4 class="m-b-0 font-16">{{ data_get($data, 'name') }}</h4>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5 class="m-b-0">{{ number_format(data_get($data, 'today')) }}</h5>
                                    @if ($data['today_previous'] > $data['today'])
                                        <span
                                            class="text-danger">{{ number_format(data_get($data, 'today_previous')) }}</span>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="m-b-0">{{ number_format(data_get($data, 'monthly')) }}</h5>
                                    @if ($data['monthly_previous'] > $data['monthly'])
                                        <span
                                            class="text-danger">{{ number_format(data_get($data, 'monthly_previous')) }}</span>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="m-b-0">{{ number_format(data_get($data, 'annual')) }}</h5>
                                    @if ($data['annual_previous'] > $data['annual'])
                                        <span
                                            class="text-danger">{{ number_format(data_get($data, 'annual_previous')) }}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
