<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style>
        table{
            width: 100%;
            border:1px solid #000000;
            border-collapse: collapse;
        }
        table td{
            border:1px solid #000000;
            padding: 3px
        }
        .title{
            text-align: center;
        }
        .logo{
            text-align: center;
            width: 100px;
        }
        .card {
            border: 3px;
            text-align: right;
        }
    </style>
</head>
<body>
    <div>
        <img class = "logo" src="{{ public_path('assets/images/warung_smart.jpeg') }}" alt="Logo Warung Smart">
        <div class="card">
            <span>{{ $user->name }}</span>
            <br>
            <span>{{ $user->email }}</span>
            <br>
            <span>{{ $user->phone }}</span>
            <br>
        </div>
    </div>
    <div>
        <h2 class="title">{{ $title }}</h2>
        <p>Laporan {{ $start_date.' - '.$end_date }}</p>
    </div>
    <table>
        <thead>
            <tr>
                <td>No</td>
                <td>Tanggal</td>
                <td>Pelanggan</td>
                <td>Produk</td>
                <td>Harga</td>
            </tr>
        </thead>
        <tbody>
            @foreach (data_get($datas, []) as $data)
            <tr>
                <td>{{ $loop->iteration.'.' }}</td>
                <td>{!! date('d-M-Y', strtotime($data->created_at)) !!}</td>
                <td>{{ $data->user->name }}</td>
                <td>{{ $data->product->name }}</td>
                <td>
                    Rp. {{ number_format($data->price,0, ',' , '.') }}
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="4" align="center">Total:</td>
                <td>
                    Rp. {{ number_format($sum_total,0, ',' , '.') }}
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
