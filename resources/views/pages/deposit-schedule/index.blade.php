@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">{{ ucfirst($module) }}</h4>
                                <h6 class="card-subtitle">Manage {{ $module }}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Agent</th>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>Day</th>
                                        <th>Sla</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route($module . '.index') }}',
                columns: [{
                        data: 'agent_name',
                        name: 'agent_name'
                    }, {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'deposit_amount',
                        name: 'deposit_amount'
                    },
                    {
                        data: 'deposit_day',
                        name: 'deposit_day'
                    },
                    {
                        data: 'sla',
                        name: 'sla'
                    },
                ]
            });
        });
    </script>
@endpush
