@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">{{ module_title($module) }}</h4>
                                <h6 class="card-subtitle">Detail {{ module_title($module) }}</h6>
                            </div>
                            <div class="col-md-4" align="right">
                                <a href="{{ route($module . '.index') }}" class="btn btn-danger btn-lg"><i
                                        class="m-r-10 mdi mdi-backspace"></i>Back</a>
                            </div>
                        </div>
                        <hr>
                        <div class="form form-horizontal">
                            @foreach ($shows as $item)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ ucfirst($item) }}</label>
                                    <div class="col-sm-10">
                                        @switch($item)
                                            @case('agent')
                                            @case('product')
                                                {{ $detail->{$item}->name }}
                                            @break

                                            @case('type')
                                                {{ data_get(config('status.booking_type'), $detail->{$item}) }}
                                            @break

                                            @case('status')
                                                {{ data_get(config('status.active'), $detail->{$item}) }}
                                            @break

                                            @default
                                                {{ $detail->{$item} }}
                                        @endswitch
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">History</h4>
                                <h6 class="card-subtitle">Booking History</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-inverse" style="width: 100%">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>Date</th>
                                            <th>Order</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach (data_get($detail, 'booking_histories', []) as $history)
                                            <tr>
                                                <td>{{ date('Y-m-d', strtotime($history->date)) }}</td>
                                                <td>{{ data_get($history, 'order.reference_code') }}</td>
                                                {{-- <td>{{ data_get(config('status.order'), data_get($history, 'order.status'), 0) }} --}}
                                                    <td></td>
                                                </td>
                                                <td>
                                                    @if ($history->order_id == null)
                                                        <a href="{{ route('booking.generate', $history->id) }}"
                                                            class="btn btn-primary">Generate</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
