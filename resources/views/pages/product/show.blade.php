@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">{{ module_title($module) }}</h4>
                                <h6 class="card-subtitle">Detail {{ module_title($module) }}</h6>
                            </div>
                            <div class="col-md-4" align="right">
                                <a href="{{ route($module . '.index') }}" class="btn btn-danger btn-lg"><i
                                        class="m-r-10 mdi mdi-backspace"></i>Back</a>
                            </div>
                        </div>
                        <hr>
                        <div class="form form-horizontal">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Category</label>
                                <div class="col-sm-10">
                                    {{ data_get($detail, 'category.name') }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    {{ $detail->name }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    {{ $detail->price }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">SKU</label>
                                <div class="col-sm-10">
                                    {{ $detail->sku }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    {{ $detail->description }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Status</label>
                                <div class="col-sm-10">
                                    {{ $detail->status }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="mb-3">Grafik</h5>
                        <div class="d-flex m-b-30 no-block">
                            <h5 class="card-title m-b-0 align-self-center chart-type">
                                <a data-value="order_monthly" class="btn btn-primary btn-report" href="#"
                                    role="button">Order Bulan Ini</a>
                                <a data-value="order_yearly" class="btn btn-primary btn-report" href="#" role="button">Order
                                    Tahun Ini</a>
                                <a data-value="booking_monthly" class="btn btn-primary btn-report" href="#"
                                    role="button">Booking Bulan Ini</a>
                                <a data-value="booking_yearly" class="btn btn-primary btn-report" href="#"
                                    role="button">Booking Tahun Ini</a>
                            </h5>

                            <div class="ml-auto" id="color-info"></div>
                        </div>

                        <div id="line-report"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">User Booking</h4>
                                <h6 class="card-subtitle">List</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Number</th>
                                        <th>Qty</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link href="{{ asset('assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
@endpush

@push('js')
    <script>
        var dataTable = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route($module . '.show', $detail->id) }}',
            columns: [{
                    data: 'user.name',
                    name: 'user.name'
                },
                {
                    data: 'number',
                    name: 'number'
                },
                {
                    data: 'qty',
                    name: 'qty'
                },
                {
                    data: 'type',
                    name: 'type'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                }
            ]
        });
    </script>

    <script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/libs/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(function() {
            var start = moment().subtract(1, 'months');
            var end = moment();

            $(".btn-report").click(function() {
                if ($(this).hasClass('btn-primary')) {
                    $(".btn-report").removeClass('btn-secondary').addClass('btn-primary');
                    $(this).removeClass('btn-primary').addClass('btn-secondary');
                    setBar();
                }
            });
            $(".chart-type a:first").trigger('click');
        });

        function setBar() {
            $("#line-report").empty();

            var active = $(".btn-report.btn-secondary").data('value');
            var chart = Morris.Line({
                element: 'line-report',
                xkey: 'date',
                ykeys: [active],
                labels: [active],
                xLabelAngle: 60,
                xLabelMargin: 10,
                lineColors: ['#7460ee'],
            });

            $.getJSON("{{ route($module . '.chart', $detail->id) }}", {
                    type: active,
                },
                function(data, textStatus, jqXHR) {
                    chart.setData(data);
                    $("#color-info").html('<ul class="list-inline text-right">' +
                        '<li class="list-inline-item">' +
                        '<h5><i class="fa fa-circle m-r-5 text-primary"></i>' + active + '</h5>' +
                        '</li>' +
                        '</ul>');
                }
            );
        }
    </script>
@endpush
