<?php

namespace App\Models;

class Booking extends BaseModel
{
    protected $fillable = [
        'id',
        'user_id',
        'agent_id',
        'product_id',
        'number',
        'qty',
        'type',
        'day',
        'status',
        'created_at',
        'updated_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function booking_histories()
    {
        return $this->hasMany(BookingHistory::class)->orderBy('date', 'DESC');
    }
}
