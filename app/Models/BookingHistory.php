<?php

namespace App\Models;

class BookingHistory extends BaseModel
{
    protected $fillable = [
        'id',
        'booking_id',
        'order_id',
        'date',
        'created_at',
        'updated_at',
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
