<?php

namespace App\Models;

class Order extends BaseModel
{
    protected $fillable = [
        'id',
        'user_id',
        'reference_code',
        'type',
        'product_id',
        'number',
        'reference_id',
        'sku',
        'status',
        'price',
        'base_price',
        'qty',
        'total',
        'profit',
        'margin_agent',
        'agent_id',
        'unit',
        'data',
        'created_at',
        'updated_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order_histories()
    {
        return $this->hasMany(OrderHistory::class);
    }

    public function last_history()
    {
        return $this->hasOne(OrderHistory::class)->where(['status' => 1]);
    }
}
