<?php

namespace App\Models;

class OrderHistory extends BaseModel
{
    protected $fillable = [
        'id',
        'order_id',
        'status',
        'description',
        'data',
        'created_at',
        'updated_at'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
