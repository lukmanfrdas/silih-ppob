<?php

namespace App\Models;

class Balance extends BaseModel
{
    protected $fillable = [
        'id',
        'type',
        'amount',
        'saldo',
        'saldo_before',
        'reference',
        'notes',
        'data',
        'transaction_date',
        'status',
        'user_id',
        'category',
        'is_topup',
        'agent_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id')->withTrashed();
    }
}
