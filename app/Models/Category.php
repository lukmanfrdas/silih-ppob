<?php

namespace App\Models;

class Category extends BaseModel
{
    protected $fillable = [
        'id',
        'parent_id',
        'name',
        'image',
        'status',
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id')->withTrashed();
    }
}
