<?php

namespace App\Models;

class Product extends BaseModel
{
    protected $fillable = [
        'id',
        'category_id',
        'name',
        'price',
        'profit',
        'margin_agent',
        'image',
        'sku',
        'description',
        'status',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
