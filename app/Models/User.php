<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, HasRoles, Uuids, HasApiTokens, HasFactory, Notifiable;

    protected $keyType = 'string';
    protected $primaryKey = 'id';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'type',
        'password',
        'google_id',
        'email_verified_at',
        'agent_id',
        'deposit_day',
        'deposit_amount',
        'agent_approval',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id')->withTrashed();
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'user_id')->orderBy('created_at', 'DESC');
    }

    public function saldo()
    {
        return $this
            ->hasOne(Balance::class, 'user_id', 'id')
            ->orderBy('created_at', 'DESC')
            ->where('category', 'SALDO')
            ->where('status', 1);
    }

    public function deposits()
    {
        return $this->hasMany(Balance::class, 'user_id')->whereIsTopup('1')->orderBy('created_at', 'DESC');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id')->orderBy('created_at', 'DESC');
    }

    public function agent_bookings()
    {
        return $this->hasMany(Booking::class, 'agent_id')->orderBy('created_at', 'DESC');
    }

    public function agent_users()
    {
        return $this->hasMany(User::class, 'agent_id')->orderBy('created_at', 'DESC');
    }
}
