<?php

namespace App\Services;

use App\Models\Balance;
use Illuminate\Support\Facades\Auth;

class BalanceService
{
    public static function getBalance($data = [])
    {
        $data = array_merge([
            'user_id' => data_get(Auth::user(), 'id'),
            'category' => 'SALDO'
        ], $data);
        $balance = Balance::where($data)->where('status', 1)->orderBy('created_at', 'DESC')->first();
        return ($balance) ? $balance->saldo : 0;
    }

    public static function getApprovalCheck($user_id)
    {
        return Balance::where(['user_id' => $user_id])->where('status', 2)->exists();
    }
}
