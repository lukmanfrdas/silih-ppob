<?php

namespace App\Services;

use App\Models\Order;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class WhatsappService
{
    public static function order($order_id)
    {
        $order = Order::find($order_id);

        $phone = data_get($order, 'user.phone');
        $text = '*SILIH INFO* Transaksi atas nama ' . data_get($order, 'user.name', '-') . ' telah berhasil, produk ' . data_get($order, 'product.name') . ' dengan harga ' . number_format(data_get($order, 'total'));
        if (data_get($order, 'type') == 'pascabayar') {
            $text .= ' dan SN: ' . data_get(json_decode($order->data), 'data.sn');
        }

        $text .= ' pada ' . date('Y-m-d H:i:s', strtotime($order->created_at)) . '. Terimakasih';
        self::do($phone, $text);

        $agent_phone = data_get($order, 'agent.phone');
        self::do($agent_phone, $text);
    }

    public static function do($phone, $text, $type = 'personal')
    {
        try {
            $client = new Client(['verify' => false]);
            $response = $client->request('POST', 'https://wa-engine.onemessaging.net:4447/send-free-message/415fd71c-ebba-42a0-8a54-c5eb27adce99', [
                'json' => [
                    'sessionId' => 'warung-smart',
                    'phone' => $phone,
                    'text' => $text,
                    'type' => $type
                ]
            ]);
            $result = json_decode($response->getBody()->getContents(), true);
            if (data_get($result, 'status') == 400 || in_array($result, [[]])) {
                $data['message'] = data_get($result, 'message');
                $status = 500;
            } else {
                $data['message'] = data_get($result, 'message');
                $status = 200;
            }
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    private function getDayName($day)
    {
        $data = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        ];

        return data_get($data, $day, 'Sunday');
    }

    public static function approvalRequest($balance)
    {
        $text = "*SILIH INFO - REQUEST DEPOSIT*\n💰💰💰\n\nCustomer:\t" . data_get($balance, 'user.name') .
            "\nNominal:\t" . number_format($balance->amount) .
            "\nSales:\t" . data_get($balance, 'agent.name') .
            "\n\nSilahkan finance segera konfirmasi";
        WhatsappService::do(config('app.sales_group'), $text, 'group');

        if ($balance->status == 1) {
            self::approvalInfo($balance);
        }
    }

    public static function approvalInfo($balance)
    {
        $icon = '❌❌❌';

        if ($balance->status == 1) {
            $icon = '✅✅✅';
            Log::info('BALANCE CONFIRMATION - Confirm', ['balance' => $balance, 'user' => Auth::user()]);
        }

        if ($balance->status == 99) {
            Log::info('BALANCE CONFIRMATION - Reject', ['balance' => $balance, 'user' => Auth::user()]);
        }


        $text = "*SILIH INFO - APPROVAL DEPOSIT*\n" . $icon . "\n\nCustomer:\t" .
            data_get($balance, 'user.name') .
            "\nAgent:\t" . data_get($balance, 'agent.name') .
            "\nNominal:\t" . number_format($balance->amount) .
            "\nApproval:\t" . config('status.balance')[$balance->status] .
            "\n\nTerimakasih";

        WhatsappService::do(config('app.sales_group'), $text, 'group');
    }
}
