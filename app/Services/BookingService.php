<?php

namespace App\Services;

use App\Models\BookingHistory;

class BookingService
{
    public static function do($data)
    {
        switch ($data->type) {
            case 1:
                $day = self::getDayName($data->day);

                $i = 0;
                $date = date('Y-m-d', strtotime('next ' . $day));
                while (date('m') == date('m', strtotime($date))) {
                    BookingHistory::create(['booking_id' => $data->id, 'date' => $date]);

                    $i++;
                    $date = date('Y-m-d', strtotime('+' . $i . ' week', strtotime('next ' . $day)));
                }
                break;

            case 2:
                BookingHistory::create([
                    'booking_id' => $data->id,
                    'date' => date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-') . $data->day))),
                ]);
                break;
        }
    }

    private function getDayName($day)
    {
        $data = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        ];

        return data_get($data, $day, 'Sunday');
    }
}
