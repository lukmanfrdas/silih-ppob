<?php

namespace App\Services;

use App\Models\Balance;
use App\Models\Product;
use App\Models\User;
use App\Services\BalanceService;
use Illuminate\Support\Facades\Log;
use Svakode\Svaflazz\Svaflazz;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    public static function order($request, $module)
    {
        $log = [];

        $user = User::findOrFail(data_get($request, 'user_id'));
        $product = Product::findOrFail(data_get($request, 'product_id'));
        $balance = BalanceService::getBalance([
            'user_id' => data_get($user, 'id'),
            'category' => 'SALDO'
        ]);

        $agent_id = data_get($request, 'agent_id');
        if (Auth::user()->type == 'agent') $agent_id = Auth::user()->id;

        if ($balance >= data_get($product, 'price')) {
            $success = false;
            $input['status'] = 0;
            $input['price'] = data_get($product, 'price');

            $reference_id = time() . 'id' . substr(data_get($request, 'number'), -4);

            // PASCA BAYAR
            if (data_get($product, 'category.parent.name') == 'Pasca Bayar') {
                $checkBill = Svaflazz::CheckBill(data_get($product, 'sku'), data_get($request, 'number'), $reference_id);
                Log::info('PASCA BAYAR CHECK BILL', ['data' => $checkBill]);

                if (data_get($checkBill, 'data.rc') == '00' || data_get($checkBill, 'data.rc') == '03') {
                    $price = data_get($checkBill, 'data.selling_price', data_get($checkBill, 'data.price', 0)) + data_get($product, 'profit', 0);
                    if ($price > $balance) return ['status' => false, 'message' => 'Price: ' . number_format($price) . '. Sisa saldo anda tidak mencukupi'];

                    $topupData = Svaflazz::PayBill(data_get($product, 'sku'), data_get($request, 'number'), data_get($checkBill, 'data.ref_id'));
                    if (data_get($topupData, 'data.rc') == '00' || data_get($topupData, 'data.rc') == '03') {
                        if (data_get($topupData, 'data.rc') == '00') {
                            $input['status'] = 1;
                        }

                        $success = true;
                        $log = $topupData;

                        $selling_price = data_get($topupData, 'data.selling_price', 0);
                        $price = data_get($topupData, 'data.price', 0);
                        $commission = $selling_price - $price;
                        $product_profit = data_get($product, 'profit', 0);

                        $input['base_price'] = $price;
                        $input['price'] = ($input['base_price'] * 1) + $commission + $product_profit;
                        $input['reference_id'] = data_get($topupData, 'data.ref_id');
                        $input['qty'] = 1;
                        $input['type'] = 'pascabayar';
                        $input['profit'] = $commission + $product_profit;
                    } else {
                        Log::error('ERROR PASCA BAYAR', ['data' => $topupData]);
                        return ['status' => false, 'message' => 'Silahkan coba beberapa saat lagi'];
                    }
                } else {
                    Log::error('ERROR PASCA BAYAR', ['data' => $checkBill]);
                    return ['status' => false, 'message' => 'Silahkan coba beberapa saat lagi'];
                }
            }

            // PRA BAYAR
            if (data_get($product, 'category.parent.name') == 'Pra Bayar') {
                $topupData = Svaflazz::topup(data_get($product, 'sku'), data_get($request, 'number'), $reference_id);
                if (data_get($topupData, 'data.rc') == '00' || data_get($topupData, 'data.rc') == '03') {
                    if (data_get($topupData, 'data.rc') == '00') {
                        $input['status'] = 1;
                    }

                    $success = true;
                    $log = $topupData;

                    $input['base_price'] = data_get($topupData, 'data.price');
                    $input['reference_id'] = data_get($topupData, 'data.ref_id');
                    $input['qty'] = 1;
                    $input['type'] = 'prabayar';
                    $input['profit'] = $input['price'] - $input['base_price'];
                } else {
                    Log::error('ERROR PRA BAYAR', ['data' => $topupData]);
                    return ['status' => false, 'message' => 'Silahkan coba beberapa saat lagi'];
                }
            }

            // PRODUCT
            if (data_get($product, 'category.name') == 'Kebutuhan Primer') {
                $success = true;

                $input['status'] = 1;
                $input['base_price'] = data_get($product, 'price');
                $input['qty'] = data_get($request, 'qty', 0);
                $input['type'] = 'product';
                $input['profit'] = data_get($product, 'profit', 0) * $input['qty'];
            }

            if ($success) {
                $total = $input['qty'] * $input['price'];

                $input['reference_code'] = data_get($product, 'sku') . date('Ymd') . rand(1, 100000000);
                $input['user_id'] = data_get($user, 'id');
                $input['product_id'] = data_get($product, 'id');
                $input['number'] = data_get($request, 'number');
                $input['sku'] = data_get($product, 'sku');
                $input['agent_id'] = $agent_id;
                $input['margin_agent'] = data_get($product, 'margin_agent');
                $input['total'] = $total;
                $input['data'] = json_encode($log);
                $post = Order::create($input);

                Balance::create([
                    'amount' => $post->total,
                    'type' => 'debit',
                    'notes' => 'Pembayaran Order ' . data_get($post, 'reference_code'),
                    'saldo' => $balance - $post->total,
                    'saldo_before' => $balance,
                    'status' => 1,
                    'reference' => 'INV/ORDER/' . $post->reference_code,
                    'user_id' => $post->user_id,
                    'category' => 'SALDO',
                    'transaction_date' => date('Y-m-d H:i:s'),
                    'is_topup' => false
                ]);

                gilog("Create " . $module, $post, $input);
                return ['status' => true, 'data' => $post];
            } else {
                return ['status' => false, 'message' => 'Anda gagal membeli produk ' . data_get($product, 'name')];
            }
        } else {
            return ['status' => false, 'message' => 'Sisa saldo anda tidak mencukupi'];
        }
    }
}
