<?php

namespace App\Imports;

use App\Models\GroupPhone;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GroupPhoneImport implements ToModel, WithHeadingRow
{

    public function __construct($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!empty($row['phone'])) {
            return GroupPhone::updateOrCreate([
                'group_id' => $this->group_id,
                'phone' => trim($row['phone'])
            ], [
                'name' => trim($row['name']),
                'phone' => trim($row['phone'])
            ]);
        }
    }
}
