<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('product');
        $unique_sku = (!empty($id)) ? 'unique:products,sku,' . $id : 'unique:products,sku';

        return [
            'category_id' => 'required',
            'name' => 'required',
            'price' => 'required||numeric',
            'margin_agent' => 'required|numeric',
            // 'image' => 'required',
            'sku' => 'required|' . $unique_sku,
            'description' => 'required',
            'status' => 'required',
        ];
    }
}
