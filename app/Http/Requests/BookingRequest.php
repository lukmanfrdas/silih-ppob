<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'qty' => 'required',
            'type' => 'required',
            'status' => 'required',
            'day' => 'required|numeric'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->type == 1 && $this->day > 7) {
                $validator->errors()->add('day', 'Maksimal hari ke 7 dalam seminggu');
            }

            if ($this->type == 2 && $this->day > 28) {
                $validator->errors()->add('day', 'Maksimal hari ke 28 dalam sebulan');
            }

            if ($this->product_id) {
                $product = Product::find($this->product_id);
                if (in_array(data_get($product, 'category.parent.name'), ['Pra Bayar', 'Pasca Bayar'])) {
                    if ($this->number == null) {
                        $validator->errors()->add('number', 'Wajib diisi');
                    }
                }
            }

            if ($this->user_id == null && in_array(Auth::user()->type, ['super-admin', 'agent'])) {
                $validator->errors()->add('user_id', 'Wajib diisi');
            }

            return $validator;
        });
    }
}
