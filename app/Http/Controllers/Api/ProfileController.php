<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function index()
    {
        try {
            $data = Auth::user();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new UserResource($data)
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function update(Request $request)
    {
        try {
            $data = Auth::user();
            $data->name = data_get($request, 'name');
            $data->email = data_get($request, 'email');
            $data->phone = data_get($request, 'phone');
            $data->address = data_get($request, 'address');
            $data->save();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new UserResource($data)
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
