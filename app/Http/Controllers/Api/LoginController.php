<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        try {
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
                $data = auth()->user();
                $data['token'] = $data->createToken('token')->accessToken;

                $response = [
                    'code' => 200,
                    'message' => 'successfully',
                    'data' => new AuthResource($data)
                ];
            } else {
                $response = [
                    'code' => 401,
                    'message' => 'Email dan password Anda salah!',
                    'data' => null
                ];
            }
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
