<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BalanceResource;
use App\Services\BalanceService;

class BalanceController extends Controller
{

    public function index()
    {
        try {
            $data = [
                'balance' => BalanceService::getBalance()
            ];

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new BalanceResource($data)
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
