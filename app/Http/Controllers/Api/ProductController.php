<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductController extends Controller
{

    public function index()
    {
        try {
            $data = Product::query()
                ->whereStatus(1)
                ->orderBy('name', 'ASC')
                ->paginate(20);

            return ProductResource::collection($data);
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
