<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BalanceResource;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\BalanceService;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{

    public function index()
    {
        try {
            $data = Order::query()
                ->whereUserId(Auth::user()->id)
                ->whereStatus(1)
                ->orderBy('created_at', 'DESC')
                ->paginate(20);

            return OrderResource::collection($data);
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
