<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookingResource;
use App\Http\Resources\OrderResource;
use App\Models\Booking;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{

    public function index()
    {
        try {
            $data = Booking::query()
                ->whereUserId(Auth::user()->id)
                ->whereStatus(1)
                ->orderBy('created_at', 'DESC')
                ->paginate(20);

            return BookingResource::collection($data);
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function store(Request $request)
    {
        try {
            $user = Auth::user();

            $input = $request->all();
            $input['user_id'] = $user->id;
            $input['agent_id'] = $user->agent_id;
            $input['status'] = 1;

            $data = Booking::create($input);
            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new BookingResource($data)
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }

    public function destroy($id)
    {
        try {
            $data = Booking::findOrFail($id);
            $data->delete();

            $response = [
                'code' => 200,
                'message' => 'successfully',
                'data' => new BookingResource($data)
            ];
        } catch (\Exception $ex) {
            $response = [
                'code' => 500,
                'message' => $ex->getMessage(),
                'data' => null
            ];
        }

        return response()->json($response, $response['code']);
    }
}
