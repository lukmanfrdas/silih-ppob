<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Balance;
use App\Models\User;
use App\Services\BalanceService;
use App\Services\WhatsappService;
use App\Traits\XIForm;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JsValidator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use XIForm;
    public function __construct(User $user, FormBuilder $formBuilder)
    {
        $this->repository = $user;
        $this->formBuilder = $formBuilder;
        $this->module = 'user';
        $this->permissions = ["view", "create", "update", "delete"];
        $this->formRequest = 'App\Http\Requests\UserRequest';
        $this->form = "App\Forms\UserForm";
        View::share(['module' => $this->module]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $formTopupSaldo = $this->formBuilder->create('App\Forms\TopupSaldoForm', [
            'method' => 'POST'
        ]);

        $formCutSaldo = $this->formBuilder->create('App\Forms\CutSaldoForm', [
            'method' => 'POST'
        ]);

        if ($request->ajax()) {
            $data = $this->repository->query();
            if ($request->user()->type == 'super-admin') {
                $data = $data->whereIn('type', ['user', 'super-admin']);
            } else {
                $data = $data
                    ->whereIn('type', ['user'])
                    ->whereAgentId($request->user()->id);
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) use ($request, $formTopupSaldo, $formCutSaldo) {
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                    $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                    $buttons[] = ['type' => 'reject', 'route' => route($this->module . '.destroy', $data->id), 'confirm' => 'Are you sure?', 'label' => 'Delete'];

                    if ($request->user()->can('balance.topup.saldo')) {
                        $buttons[] = [
                            'label' => 'Topup Saldo',
                            'route' => route($this->module . '.topup-saldo', $data->id),
                            'confirm' => 'Are you sure?',
                            'form' => $formTopupSaldo,
                            'class' => 'btn-primary',
                            'action' => 'primary',
                            'validation' => "App\Http\Requests\TopupRequest"
                        ];
                    }

                    if ($request->user()->can('balance.cut.saldo')) {
                        $buttons[] = [
                            'label' => 'Pengurangan Saldo',
                            'route' => route($this->module . '.cut-saldo', $data->id),
                            'confirm' => 'Are you sure?',
                            'form' => $formCutSaldo,
                            'class' => 'btn-primary',
                            'action' => 'primary',
                            'validation' => "App\Http\Requests\TopupRequest"
                        ];
                    }

                    if ($request->user()->type == 'super-admin') {
                        $buttons[] = [
                            'label' => 'Login sebagai ' . $data->name,
                            'route' => route($this->module . '.login-as', $data->id),
                            'confirm' => 'Are you sure?',
                            'form' => [],
                            'method' => 'PATCH',
                            'action' => 'info'
                        ];
                    }

                    return $this->icon_button($buttons, true);
                })
                ->addColumn('role', function ($data) {
                    return '<label class="label label-info">' . $data->roles()->pluck('name')->implode('</label> <label class="label label-info">') . '</label>';
                })
                ->addColumn('user', function ($data) {
                    return data_get($data, 'user.name');
                })
                ->addColumn('saldo', function ($data) {
                    return number_format(data_get($data, 'saldo.saldo', 0));
                })
                ->addColumn('created_at', function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->created_at));
                })
                ->addColumn('agent_name', function ($data) {
                    return data_get($data, 'agent.name');
                })
                ->addColumn('deposit_day', function ($data) {
                    return ($data->deposit_day) ? 'Day ' . $data->deposit_day : '';
                })
                ->rawColumns(['action', 'role', 'deposit_day'])
                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator'] = JsValidator::formRequest($this->formRequest);
        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        try {
            DB::transaction(function () use ($request) {
                $input = $request->all();

                $user = Auth::user();
                switch ($user->type) {
                    case 'super-admin':
                        $user = User::find($input['agent_id']);
                        $input['agent_id'] = $user->id;
                        break;
                    case 'agent':
                        $input['agent_id'] = $user->id;
                        $input['type'] = 'user';
                        break;
                }

                if ($input['password']) $input['password'] = Hash::make($input['password']);
                $post = $this->repository->create($input);
                $this->assignRole($post, $input['type']);

                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success create ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->with(['bookings.product', 'deposits.agent'])->find($id);
        $data['detail'] = $get;
        $detail = new User();
        $data['shows'] = $detail->getFillable();

        if ($request->ajax()) {
            if (data_get($request, 'type') == 'booking') {
                $data = $get->bookings;
                return DataTables::of($data)
                    ->addColumn('day', function ($data) {
                        return 'Day ' . $data->day;
                    })
                    ->addColumn('status', function ($data) {
                        return data_get(config('status.active'), $data->status);
                    })
                    ->addColumn('type', function ($data) {
                        return data_get(config('status.booking_type'), $data->type);
                    })
                    ->addColumn('created_at', function ($data) {
                        return date('Y-m-d H:i:s', strtotime($data->created_at));
                    })
                    ->rawColumns(['day'])
                    ->make();
            }

            if (data_get($request, 'type') == 'deposit') {
                $data = $get->deposits;
                return DataTables::of($data)
                    ->addColumn('status', function ($data) {
                        return data_get(config('status.balance'), $data->status);
                    })
                    ->addColumn('created_at', function ($data) {
                        return date('Y-m-d H:i:s', strtotime($data->created_at));
                    })
                    ->rawColumns(['day'])
                    ->make();
            }
        }

        return view('pages.' . $this->module . '.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $get->password = "";
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);
        $data['detail'] = $get;

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        try {
            DB::transaction(function () use ($request, $id) {
                $input = $request->all();

                $user = Auth::user();
                switch ($user->type) {
                    case 'super-admin':
                        $user = User::find($input['agent_id']);
                        $input['agent_id'] = $user->id;
                        break;
                    case 'agent':
                        $input['agent_id'] = $user->id;
                        $input['type'] = 'user';
                        break;
                }

                if ($input['password']) {
                    $input['password'] = Hash::make($input['password']);
                } else {
                    unset($input['password']);
                }

                $post = $this->repository->find($id);
                $post->update($input);
                $this->assignRole($post, $input['type']);

                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.delete')) return notPermited();

        try {
            DB::transaction(function () use ($id) {
                $get = $this->repository->find($id);
                $get->delete($id);
                gilog("Delete " . $this->module, $get, ['notes' => @request('notes')]);
            });
            $data['message'] = 'Success delete ' . $this->module;
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function assignRole($model, $roles = '')
    {
        foreach ($model->roles as $key => $value) {
            $model->removeRole($value);
        }
        if (isset($roles)) {
            $model->assignRole($roles);
        }
    }

    public function loginAs(Request $request, $id)
    {
        if (Auth::user()->type == 0) {
            $request->session()->put('admin_login', Auth::user()->id);
        } else {
            $request->session()->forget('admin_login');
        }
        Auth::loginUsingId($id);
        $data['status'] = 1;
        $data['redirect'] = url('/');
        return response()->json($data, 200);
    }

    public function topupSaldo(Request $request, $id)
    {
        if (!Auth::user()->can('balance.topup.saldo')) return notPermited('json');

        $check = BalanceService::getApprovalCheck($id);
        if ($check) {
            $data['status'] = 0;
            $data['code'] = 400;
            $data['message'] = 'Silahkan hubungi finance untuk Approval topup terakhir user ini';

            return response()->json($data, $data['code']);
        }

        try {
            DB::beginTransaction();
            $saldo = BalanceService::getBalance([
                'user_id' => $id,
                'category' => 'SALDO'
            ]);
            $user = User::find($id);
            $agent = Auth::user();
            $agent_id = $agent->id;
            if ($agent->type == 'super-admin') $agent_id = $request->agent_id;

            $status = 2; // Waiting Confirmation
            if ($agent->agent_approval == false) {
                $status = 1;
            }

            $data = [
                'amount' => $request->amount,
                'type' => 'credit',
                'notes' => 'Topup Saldo oleh ' . $agent->name . ' (' . $request->notes . ')',
                'saldo' => $saldo + $request->amount,
                'saldo_before' => $saldo,
                'status' => $status,
                'reference' => 'TOPSLD/' . date("YmdHi") . '/' . $agent_id,
                'user_id' => $id,
                'category' => 'SALDO',
                'transaction_date' => date('Y-m-d H:i:s'),
                'is_topup' => true,
                'agent_id' => $agent_id
            ];
            $post = Balance::create($data);
            gilog("topupSaldo " . $this->module, $post, $data);

            $data['code'] = 200;
            $data['status'] = 1;
            $data['message'] = 'Berhasil melakukan topup, silahkan hubungi finance untuk konfirmasi';
            DB::commit();

            WhatsappService::approvalRequest($post);
        } catch (\Exception $ex) {
            DB::rollback();
            $data['code'] = 400;
            $data['status'] = 0;
            $data['message'] = $ex->getMessage();
        }

        return response()->json($data, $data['code']);
    }

    public function cutSaldo(Request $request, $id)
    {
        if (!Auth::user()->can('balance.cut.saldo')) return notPermited('json');

        try {
            DB::beginTransaction();
            $saldo = BalanceService::getBalance([
                'user_id' => $id,
                'category' => 'SALDO'
            ]);
            if ($request->amount == 0) $request->amount = $saldo;
            $user = Auth::user();
            $data = [
                'amount' => $request->amount,
                'type' => 'debit',
                'notes' => 'Pengurangan Saldo oleh ' . $user->name . ' (' . $request->notes . ')',
                'saldo' => $saldo - $request->amount,
                'saldo_before' => $saldo,
                'status' => 1,
                'reference' => 'TOPSLD/' . date("YmdHi") . '/' . $user->id,
                'user_id' => $id,
                'category' => 'SALDO',
                'transaction_date' => date('Y-m-d H:i:s'),
                'is_topup' => true,
            ];
            $post = Balance::create($data);
            gilog("cutSaldo " . $this->module, $post, $data);
            $data['status'] = 1;
            $data['message'] = 'Berhasil mengurangi saldo';
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $data['status'] = 0;
            $data['message'] = $ex->getMessage();
        }

        return response()->json($data, 200);
    }
}
