<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Booking;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->type == 'user') {
            return view('home_user');
        }

        $today = date('Y-m-d');
        $month = date('m');
        $year = date('Y');

        $today_previous = date('Y-m-d', strtotime('-1 month'));
        $month_previous = date('m', strtotime('-1 month'));
        $year_previous = date('Y', strtotime('-1 year'));

        $order = Order::query()->whereStatus(1);
        $user = User::whereType('user');
        $balance = Balance::whereIsTopup(1);
        $booking = Booking::query()
            ->selectRaw('sum(price * qty) as total, bookings.created_at')
            ->join('products', 'products.id', '=', 'bookings.product_id')
            ->where(['bookings.status' => 1]);

        if (Auth::user()->type == 'agent') {
            $user_id = Auth::user()->id;
            $order = $order->whereAgentId($user_id);
            $user = $user->whereAgentId($user_id);
            $balance = $balance->whereAgentId($user_id);
            $booking = $booking->whereAgentId($user_id);
        }

        $data['statistics'] = [
            [
                'name' => 'Jumlah Pelanggan',
                'color' => 'btn-info',
                'short' => 'JP',
                'today' =>  $user->clone()->whereDate('created_at', $today)->whereNotNull('deposit_day')->count(),
                'today_previous' => $user->clone()->whereDate('created_at', $today_previous)->whereNotNull('deposit_day')->count(),
                'monthly' => $user->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month)->whereNotNull('deposit_day')->count(),
                'monthly_previous' => $user->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month_previous)->whereNotNull('deposit_day')->count(),
                'annual' => $user->clone()->whereYear('created_at', $year)->whereNotNull('deposit_day')->count(),
                'annual_previous' => $user->clone()->whereYear('created_at', $year_previous)->whereNotNull('deposit_day')->count(),
            ],
            [
                'name' => 'Jumlah Deposit',
                'color' => 'btn-secondary',
                'short' => 'JD',
                'today' =>  $balance->clone()->whereDate('created_at', $today)->count(),
                'today_previous' => $balance->clone()->whereDate('created_at', $today_previous)->count(),
                'monthly' => $balance->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month)->count(),
                'monthly_previous' => $balance->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month_previous)->count(),
                'annual' => $balance->clone()->whereYear('created_at', $year)->count(),
                'annual_previous' => $balance->clone()->whereYear('created_at', $year_previous)->count(),
            ],
            [
                'name' => 'Jumlah Booking',
                'color' => 'btn-info',
                'short' => 'JB',
                'today' =>  $booking->clone()->whereDate('bookings.created_at', $today)->count(),
                'today_previous' => $booking->clone()->whereDate('bookings.created_at', $today_previous)->count(),
                'monthly' => $booking->clone()->whereYear('bookings.created_at', $year)->whereMonth('bookings.created_at', $month)->count(),
                'monthly_previous' => $booking->clone()->whereYear('bookings.created_at', $year)->whereMonth('bookings.created_at', $month_previous)->count(),
                'annual' => $booking->clone()->whereYear('bookings.created_at', $year)->count(),
                'annual_previous' => $booking->clone()->whereYear('bookings.created_at', $year_previous)->count(),
            ],
            [
                'name' => 'Jumlah Transaksi',
                'color' => 'btn-secondary',
                'short' => 'JT',
                'today' =>  $order->clone()->whereDate('created_at', $today)->count(),
                'today_previous' => $order->clone()->whereDate('created_at', $today_previous)->count(),
                'monthly' => $order->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month)->count(),
                'monthly_previous' => $order->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month_previous)->count(),
                'annual' => $order->clone()->whereYear('created_at', $year)->count(),
                'annual_previous' => $order->clone()->whereYear('created_at', $year_previous)->count(),
            ],
            [
                'name' => 'Total Deposit',
                'color' => 'btn-primary',
                'short' => 'TD',
                'today' =>  $balance->clone()->whereDate('created_at', $today)->sum('amount'),
                'today_previous' => $balance->clone()->whereDate('created_at', $today_previous)->sum('amount'),
                'monthly' => $balance->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month)->sum('amount'),
                'monthly_previous' => $balance->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month_previous)->sum('amount'),
                'annual' => $balance->clone()->whereYear('created_at', $year)->sum('amount'),
                'annual_previous' => $balance->clone()->whereYear('created_at', $year_previous)->sum('amount'),
            ],
            [
                'name' => 'Total Booking',
                'color' => 'btn-danger',
                'short' => 'TB',
                'today' =>  $booking->clone()->whereDate('bookings.created_at', $today)->first()->total,
                'today_previous' => $booking->clone()->whereDate('bookings.created_at', $today_previous)->first()->total,
                'monthly' => $booking->clone()->whereYear('bookings.created_at', $year)->whereMonth('bookings.created_at', $month)->first()->total,
                'monthly_previous' => $booking->clone()->whereYear('bookings.created_at', $year)->whereMonth('bookings.created_at', $month_previous)->first()->total,
                'annual' => $booking->clone()->whereYear('bookings.created_at', $year)->first()->total,
                'annual_previous' => $booking->clone()->whereYear('bookings.created_at', $year_previous)->first()->total,
            ],
            [
                'name' => 'Total Transaksi',
                'color' => 'btn-success',
                'short' => 'TT',
                'today' =>  $order->clone()->whereDate('created_at', $today)->sum('total'),
                'today_previous' => $order->clone()->whereDate('created_at', $today_previous)->sum('total'),
                'monthly' => $order->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month)->sum('total'),
                'monthly_previous' => $order->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month_previous)->sum('total'),
                'annual' => $order->clone()->whereYear('created_at', $year)->sum('total'),
                'annual_previous' => $order->clone()->whereYear('created_at', $year_previous)->sum('total'),
            ],
            [
                'name' => 'Laba Bersih',
                'color' => 'btn-default',
                'short' => 'LB',
                'today' =>  $order->clone()->whereDate('created_at', $today)->sum('profit'),
                'today_previous' => $order->clone()->whereDate('created_at', $today_previous)->sum('profit'),
                'monthly' => $order->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month)->sum('profit'),
                'monthly_previous' => $order->clone()->whereYear('created_at', $year)->whereMonth('created_at', $month_previous)->sum('profit'),
                'annual' => $order->clone()->whereYear('created_at', $year)->sum('profit'),
                'annual_previous' => $order->clone()->whereYear('created_at', $year_previous)->sum('profit'),
            ],
        ];

        return view('home', $data);
    }

    public function report(Request $request)
    {
        $data = [];
        $date_start = date('Y') . '-' . date('m') . '-01';
        $date_end = date('Y-m-t', strtotime($date_start));

        $user = Auth::user();
        if ($user->type == 'agent') $request['agent_id'] = $user->id;

        switch (data_get($request, 'type')) {
            case 'order_monthly':
                for ($i = $date_start; $i <= $date_end; $i++) {
                    $order = Order::whereDate('created_at', $i);

                    if ($agent_id = data_get($request, 'agent_id')) $order = $order->whereAgentId($agent_id);

                    array_push($data, [
                        'date' => $i,
                        'order_monthly' => $order->count()
                    ]);
                }
                break;
            case 'order_yearly':
                for ($i = 01; $i <= 12; $i++) {
                    $order = Order::whereYear('created_at', date('Y'))->whereMonth('created_at', $i);

                    if ($agent_id = data_get($request, 'agent_id')) $order = $order->whereAgentId($agent_id);

                    array_push($data, [
                        'date' => date('Y-') . str_pad($i, 2, 0, STR_PAD_LEFT),
                        'order_yearly' => $order->count()
                    ]);
                }
            case 'booking_monthly':
                for ($i = $date_start; $i <= $date_end; $i++) {
                    $booking = Booking::whereDate('created_at', $i);

                    if ($agent_id = data_get($request, 'agent_id')) $booking = $booking->whereAgentId($agent_id);

                    array_push($data, [
                        'date' => $i,
                        'booking_monthly' => $booking->count()
                    ]);
                }
                break;
            case 'booking_yearly':
                for ($i = 01; $i <= 12; $i++) {
                    $booking = Booking::whereYear('created_at', date('Y'))->whereMonth('created_at', $i);

                    if ($agent_id = data_get($request, 'agent_id')) $booking = $booking->whereAgentId($agent_id);

                    array_push($data, [
                        'date' => date('Y-') . str_pad($i, 2, 0, STR_PAD_LEFT),
                        'booking_yearly' => $booking->count()
                    ]);
                }
        }

        return response()->json($data, 200);
    }
}
