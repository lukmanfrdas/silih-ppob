<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Models\Booking;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class ProductController extends Controller
{
    private $module, $model, $form;
    protected $repository;
    use XIForm;

    public function __construct(Product $repository, FormBuilder $formBuilder)
    {
        $this->module = 'product';
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->form = 'App\Forms\ProductForm';
        $this->formRequest = 'App\Http\Requests\ProductRequest';

        View::share('module', $this->module);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        if ($request->ajax()) {
            $data = $this->repository->orderBy('created_at', 'DESC');
            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                    $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                    $buttons[] = ['type' => 'delete', 'label' => 'Delete', 'confirm' => 'Are you sure?', 'route' => route($this->module . '.destroy', $data->id)];

                    return $this->icon_button($buttons, true);
                })
                ->addColumn('category_name', function ($data) {
                    return data_get($data, 'category.name');
                })

                ->addColumn('status', function ($data) {
                    return data_get(config('status.active'), $data->status);
                })
                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator'] = JsValidatorFacade::formRequest($this->formRequest);

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        try {
            DB::transaction(function () use ($request) {
                $data = $request->all();
                $post = $this->repository->create($data);
                gilog("Create " . $this->module, $post, $data);
            });
            flash('Success create ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->with(['bookings.user'])->find($id);
        $data['detail'] = $get;
        $detail = new Product();
        $data['shows'] = $detail->getFillable();

        if ($request->ajax()) {
            $data = $get->bookings;
            return DataTables::of($data)
                ->addColumn('day', function ($data) {
                    return 'Day ' . $data->day;
                })
                ->addColumn('status', function ($data) {
                    return data_get(config('status.active'), $data->status);
                })
                ->addColumn('type', function ($data) {
                    return data_get(config('status.booking_type'), $data->type);
                })
                ->addColumn('created_at', function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->created_at));
                })
                ->rawColumns(['day'])
                ->make();
        }

        return view('pages.' . $this->module . '.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        try {
            DB::transaction(function () use ($id, $request) {
                $input = $request->all();
                $post = $this->repository->find($id);
                $post->update($input);
                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.delete')) return notPermited('json');

        try {
            DB::transaction(function () use ($id) {
                $get = $this->repository->find($id);
                $get->delete($id);
                gilog("Delete " . $this->module, $get, ['notes' => @request('notes')]);
            });
            $data['message'] = 'Success delete ' . $this->module;
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function chart(Request $request, $id)
    {
        $data = [];
        switch (data_get($request, 'type')) {
            case 'order_monthly':
                $date_start = date('Y') . '-' . date('m') . '-01';
                $date_end = date('Y-m-t', strtotime($date_start));
                for ($i = $date_start; $i <= $date_end; $i++) {
                    array_push($data, [
                        'date' => $i,
                        'order_monthly' => Order::whereDate('created_at', $i)->whereProductId($id)->count()
                    ]);
                }
                break;
            case 'order_yearly':
                $data = [];
                for ($i = 01; $i <= 12; $i++) {
                    array_push($data, [
                        'date' => date('Y-') . str_pad($i, 2, 0, STR_PAD_LEFT),
                        'order_yearly' => Order::whereYear('created_at', date('Y'))->whereMonth('created_at', $i)->whereProductId($id)->count()
                    ]);
                }
            case 'booking_monthly':
                $date_start = date('Y') . '-' . date('m') . '-01';
                $date_end = date('Y-m-t', strtotime($date_start));
                for ($i = $date_start; $i <= $date_end; $i++) {
                    array_push($data, [
                        'date' => $i,
                        'booking_monthly' => Booking::whereDate('created_at', $i)->whereProductId($id)->count()
                    ]);
                }
                break;
            case 'booking_yearly':
                $data = [];
                for ($i = 01; $i <= 12; $i++) {
                    array_push($data, [
                        'date' => date('Y-') . str_pad($i, 2, 0, STR_PAD_LEFT),
                        'booking_yearly' => Booking::whereYear('created_at', date('Y'))->whereMonth('created_at', $i)->whereProductId($id)->count()
                    ]);
                }
        }

        return response()->json($data, 200);
    }
}
