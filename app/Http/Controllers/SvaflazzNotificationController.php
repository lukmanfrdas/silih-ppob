<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Balance;
use App\Models\OrderHistory;
use App\Services\BalanceService;
use App\Services\WhatsappService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SvaflazzNotificationController extends Controller
{

    public function checkNotification(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            Log::info('INFO WebhookTopup', ['data' => $input]);

            $order = Order::whereReferenceId(data_get($input, 'data.ref_id'))->first();
            if ($order) {
                if ($order->status == 1) return;

                $agent_id = data_get($order, 'agent_id');
                $margin_agent = intval(data_get($order, 'margin_agent', 0));
                $webhook_status = data_get($input, 'data.status');
                if ($webhook_status == 'Sukses') {
                    $status = 1;

                    if ($margin_agent > 0) {
                        $saldo = BalanceService::getBalance([
                            'user_id' => $agent_id,
                            'category' => 'SALDO'
                        ]);
                        Balance::create([
                            'amount' => $margin_agent,
                            'type' => 'credit',
                            'notes' => 'Sharing Profit ' . $margin_agent . ' Order ' . data_get($order, 'reference_code'),
                            'saldo' => $saldo + $margin_agent,
                            'saldo_before' => $saldo,
                            'status' => 1,
                            'reference' => 'PRFTSHR/' . $margin_agent,
                            'user_id' => $agent_id,
                            'category' => 'SALDO',
                            'transaction_date' => date('Y-m-d H:i:s'),
                            'is_topup' => false
                        ]);
                    }
                } else if ($webhook_status == 'Pending') {
                    $status = 0;
                } else {
                    $status = 99;

                    // sum and save balance
                    $saldo = BalanceService::getBalance([
                        'user_id' => $order->user_id,
                        'category' => 'SALDO'
                    ]);
                    Balance::create([
                        'amount' => $order->total,
                        'type' => 'credit',
                        'notes' => 'Refund Order ' . data_get($order, 'reference_code'),
                        'saldo' => $saldo + $order->total,
                        'saldo_before' => $saldo,
                        'status' => 1,
                        'reference' => 'INV/ORDER/' . $order->reference_code,
                        'user_id' => $order->user_id,
                        'category' => 'SALDO',
                        'transaction_date' => date('Y-m-d H:i:s'),
                        'is_topup' => false
                    ]);
                }

                $order->status = $status;
                $order->save();

                OrderHistory::create([
                    'order_id' => data_get($order, 'id'),
                    'status' =>  $status,
                    'description' => data_get(config('status.order_history'), $status),
                    'data' => json_encode($input),
                ]);
                DB::commit();

                if (data_get($order, 'status') == 1) WhatsappService::order(data_get($order, 'id'));
            } else {
                Log::error('ERROR WebhookTopup', ['description' => 'Order Not Found', 'data' => $input]);
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('ERROR WebhookTopup', ['message' => $ex->getMessage(), 'data' => $input]);
        }
    }
}
