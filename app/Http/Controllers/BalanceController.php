<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Balance;
use App\Services\WhatsappService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class BalanceController extends Controller
{
    use XIForm;
    public function __construct(Balance $repository, FormBuilder $formBuilder)
    {
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->module = 'balance';
        $this->permissions = ["view", "create", "update", "delete"];
        View::share(['module' => $this->module]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $topupRejectForm = $this->formBuilder->create('App\Forms\TopupRejectForm', [
            'method' => 'POST'
        ]);

        if ($request->ajax()) {
            $data = $this->repository
                ->orderBy('created_at', 'desc')
                ->where(['category' => 'SALDO']);

            if (!$request->user()->can('balance.view.all')) {
                $data = $data->where('user_id', $request->user()->id);
            }

            if (data_get($request, 'type') == 'waiting-confirmation') {
                $data = $data->where('status', 2);
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) use ($request, $topupRejectForm) {
                    $buttons = [];
                    if (Auth::user()->type == 'super-admin' && data_get($request, 'type') == 'waiting-confirmation') {
                        $buttons[] = [
                            'label' => 'Confirm',
                            'route' => route('balance.confirm', ['id' => $data->id]),
                            'confirm' => 'Are you sure?',
                            'class' => 'btn-primary',
                            'action' => 'primary',
                            'method' => 'POST'
                        ];
                        $buttons[] = [
                            'label' => 'Reject',
                            'route' => route($this->module . '.reject', $data->id),
                            'confirm' => 'Are you sure?',
                            'form' => $topupRejectForm,
                            'class' => 'btn-danger',
                            'action' => 'danger'
                        ];
                    }

                    return $this->icon_button($buttons);
                })
                ->addColumn('type', function ($data) {
                    return '<span class="badge badge-' . ($data->type == 'debit' ? 'danger' : 'success') . '">' . $data->type . '</span>';
                })
                ->addColumn('status', function ($data) {
                    $badge = '';
                    switch ($data->status) {
                        case 0:
                            $badge = 'secondary';
                            break;
                        case 1:
                            $badge = 'success';
                            break;
                        case 2:
                            $badge = 'info';
                            break;
                        case 99:
                        case 999:
                            $badge = 'danger';
                            break;
                    }

                    return '<span class="badge badge-' . $badge . '">' . config('status.balance')[$data->status] . '</span>';
                })
                ->addColumn('amount', function ($data) {
                    return '<span class="text-' . ($data->type == 'debit' ? 'danger' : 'success') . '">' . ($data->type == 'debit' ? '-' : '+') . ' Rp.' . number_format($data->amount) . '</span>';
                })
                ->addColumn('saldo', function ($data) {
                    return 'Rp.' . number_format($data->saldo);
                })
                ->addColumn('agent_name', function ($data) {
                    return data_get($data, 'agent.name');
                })
                ->addColumn('notes', function ($data) use ($request) {
                    $return = $data->notes;
                    if ($request->user()->type == 'super-admin') {
                        $return .= '<br><small><a href="' . route($this->module . '.index', ['user_id' => $data->user_id]) . '">' . $data->user->email . '</a></small>';
                    }
                    return $return;
                })
                ->filterColumn('notes', function ($query, $keyword) {
                    $sql = "notes LIKE ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                })
                ->rawColumns(['action', 'type', 'status', 'amount', 'notes'])
                ->make();
        }
        return view('pages.' . $this->module . '.index', ['url' => route($this->module . '.index', ['user_id' => $request->user_id])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirm($id)
    {
        try {
            $balance = Balance::findOrFail($id);
            $balance->status = 1;
            $balance->save();

            WhatsappService::approvalInfo($balance);

            $status = 200;
            $data['status'] = 1;
            $data['message'] = "Successfully";
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }

        return response()->json($data, $status);
    }

    public function reject(Request $request, $id)
    {
        try {
            $balance = Balance::findOrFail($id);
            $balance->status = 99;
            $balance->notes = data_get($request, 'notes');
            $balance->save();

            WhatsappService::approvalInfo($balance);

            $status = 200;
            $data['status'] = 1;
            $data['message'] = "Successfully";
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }

        return response()->json($data, $status);
    }
}
