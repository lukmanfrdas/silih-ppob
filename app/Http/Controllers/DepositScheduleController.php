<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DepositScheduleController extends Controller
{
    use XIForm;
    public function __construct(User $repository, FormBuilder $formBuilder)
    {
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->module = 'deposit-schedule';
        $this->permissions = ["view", "create", "update", "delete"];
        View::share(['module' => $this->module]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repository
                ->with('agent')
                ->whereNotNull('deposit_day')
                ->orderBy('deposit_day', 'ASC')
                ->whereDoesntHave('deposits', function ($query) {
                    return $query->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'));
                });

            return DataTables::of($data)
                ->addColumn('deposit_amount', function ($data) {
                    return 'Rp.' . number_format($data->deposit_amount);
                })
                ->addColumn('sla', function ($data) {
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d', date('Y-m-') . $data->deposit_day);
                    $sla = $to->diffInDays($from, false);

                    $badge = 'danger';
                    if ($sla > 0) $badge = 'success';

                    return '<span class="badge badge-' . $badge . '">' . $sla . '</span>';
                })
                ->addColumn('agent_name', function ($data) {
                    return data_get($data, 'agent.name');
                })
                ->rawColumns(['deposit_amount', 'sla'])
                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
