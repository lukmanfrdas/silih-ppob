<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use PDF;

class GeneratepdfController extends Controller
{
    public function generatePDF(Request $request, $id){
        $start_date = '';
        $end_date = '';
        $user = User::find($id);
        $orders = $user->orders;
        if($request->start_date != null && $request->end_date != null){
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $orders = $orders->whereBetween('created_at', [$request->start_date, $request->end_date]);
        }

        $data = [
            'title' => 'Laporan Transaksi',
            'datas' => $orders,
            'date' => date('m/d/Y'),
            'user' => User::find($id),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'sum_total' => $orders->sum('price')
        ];
        $pdf = PDF::loadView('pages.user.generate-pdf', $data);
        return $pdf->stream();
        // return $pdf->download($data['title'].'-'.$data['user']->name.'.pdf');
    }
}
