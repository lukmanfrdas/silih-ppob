<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Models\Booking;
use App\Models\BookingHistory;
use App\Models\User;
use App\Services\BookingService;
use App\Services\OrderService;
use App\Traits\XIForm;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JsValidator;

class BookingController extends Controller
{
    use XIForm;
    public function __construct(Booking $model, FormBuilder $formBuilder)
    {
        $this->repository = $model;
        $this->formBuilder = $formBuilder;
        $this->module = 'booking';
        $this->permissions = ["view", "create", "update", "delete"];
        $this->formRequest = 'App\Http\Requests\BookingRequest';
        $this->form = "App\Forms\BookingForm";
        View::share(['module' => $this->module]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        if ($request->ajax()) {
            $data = $this->repository
                ->with(['user', 'product', 'agent'])
                ->select([
                    'bookings.id',
                    'bookings.user_id',
                    'bookings.agent_id',
                    'bookings.product_id',
                    'bookings.number',
                    'bookings.qty',
                    'bookings.type',
                    'bookings.day',
                    'bookings.status',
                    'bookings.created_at',
                    DB::raw('(SELECT COUNT(*) FROM booking_histories WHERE booking_id=bookings.id AND order_id IS NULL AND date <= NOW()) as waiting')
                ])
                ->orderBy('waiting', 'DESC')
                ->orderBy('bookings.created_at', 'DESC');

            switch ($request->user()->type) {
                case 'agent':
                    $data = $data->where(['bookings.agent_id' => $request->user()->id]);
                    break;

                case 'user':
                    $data = $data->where(['bookings.user_id' => $request->user()->id]);
                    break;

                default:
                    # code...
                    break;
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                    $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                    $buttons[] = ['type' => 'reject', 'route' => route($this->module . '.destroy', $data->id), 'confirm' => 'Are you sure?', 'label' => 'Delete'];

                    return $this->icon_button($buttons, true);
                })
                ->addColumn('type', function ($data) {
                    return data_get(config('status.booking_type'), $data->type);
                })
                ->addColumn('status', function ($data) {
                    return data_get(config('status.active'), $data->status);
                })
                ->addColumn('waiting', function ($data) {
                    return $data->waiting;
                })
                ->rawColumns(['action', 'role'])
                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator'] = JsValidator::formRequest($this->formRequest);
        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        DB::beginTransaction();
        try {
            $input = $request->all();

            $user = Auth::user();
            switch ($user->type) {
                case 'super-admin':
                    $user = User::find($input['user_id']);
                    $input['agent_id'] = $user->agent_id;
                    break;
                case 'agent':
                    $input['agent_id'] = $user->id;
                    break;
                case 'user':
                    $input['user_id'] = $user->id;
                    $input['agent_id'] = $user->agent_id;
                    break;
            }

            $post = $this->repository->create($input);
            BookingService::do($post);

            DB::commit();
            gilog("Create " . $this->module, $post, $input);
            flash('Success create ' . $this->module)->success();
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->with(['agent'])->find($id);
        $data['detail'] = $get;
        $data['shows'] = [
            'agent',
            'product',
            'number',
            'qty',
            'type',
            'day',
            'status',
        ];
        return view('pages.' . $this->module . '.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $get->password = "";
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);
        $data['detail'] = $get;

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookingRequest $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        DB::beginTransaction();
        try {
            $input = $request->all();

            $user = Auth::user();
            switch ($user->type) {
                case 'super-admin':
                    $agent = User::find($input['user_id']);
                    $input['agent_id'] = $agent->id;
                    break;
                case 'agent':
                    $input['agent_id'] = $user->id;
                    break;
                case 'user':
                    $input['user_id'] = $user->id;
                    $input['agent_id'] = $user->agent_id;
                    break;
            }

            $post = $this->repository->find($id);
            $post->update($input);

            BookingHistory::whereBookingId($id)->whereNull('order_id')->forceDelete();
            BookingService::do($post);

            DB::commit();
            gilog("Create " . $this->module, $post, $input);
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.delete')) return notPermited();

        try {
            DB::transaction(function () use ($id) {
                $get = $this->repository->find($id);
                if (count($get->booking_histories) > 0) $get->booking_histories()->delete();
                $get->delete($id);
                gilog("Delete " . $this->module, $get, ['notes' => @request('notes')]);
            });
            $data['message'] = 'Success delete ' . $this->module;
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function generate(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.generate')) return notPermited();

        DB::beginTransaction();
        try {
            $booking_history = BookingHistory::findOrFail($id);

            $order = OrderService::order([
                'user_id' => data_get($booking_history, 'booking.user_id'),
                'product_id' => data_get($booking_history, 'booking.product_id'),
                'number' => data_get($booking_history, 'booking.number'),
                'qty' => data_get($booking_history, 'booking.qty'),
            ], $this->module);

            if ($order['status']) {
                $booking_history->update(['order_id' => data_get($order, 'data.id')]);
                DB::commit();
                flash('Success create ' . $this->module)->success();
            } else {
                DB::rollBack();
                flash($order['message'])->error();
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }
        return redirect()->back();
    }
}
