<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderProductRequest;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Order;
use App\Services\OrderService;
use App\Services\WhatsappService;
use Illuminate\Support\Facades\DB;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class OrderProductController extends Controller
{
    private $module, $model, $form;
    protected $repository;
    use XIForm;

    public function __construct(Order $repository, FormBuilder $formBuilder)
    {
        $this->module = 'order-product';
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->form = 'App\Forms\OrderProductForm';
        $this->formRequest = 'App\Http\Requests\OrderProductRequest';

        View::share('module', $this->module);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        if ($request->ajax()) {
            $data = $this->repository
                ->whereType('product')
                ->orderBy('created_at', 'DESC');

            $user = $request->user();
            switch ($user->type) {
                case 'agent':
                    $data = $data->whereAgentId($user->id);
                    break;
                case 'user':
                    $data = $data->whereUserId($user->id);
                    break;
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                    // $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                    // $buttons[] = ['type' => 'reject', 'route' => route($this->module . '.destroy', $data->id), 'confirm' => 'Are you sure?', 'label' => 'Delete'];

                    return $this->icon_button($buttons);
                })
                ->addColumn('user_name', function ($data) {
                    return data_get($data, 'user.name');
                })
                ->addColumn('agent_name', function ($data) {
                    return data_get($data, 'agent.name');
                })
                ->addColumn('user_name', function ($data) {
                    return data_get($data, 'user.name');
                })
                ->addColumn('product_name', function ($data) {
                    return data_get($data, 'product.name');
                })
                ->addColumn('total', function ($data) {
                    return number_format($data->total);
                })
                ->addColumn('created_at', function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->created_at));
                })
                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator'] = JsValidatorFacade::formRequest($this->formRequest);
        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderProductRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        DB::beginTransaction();
        try {
            $order = OrderService::order($request, $this->module);

            if ($order['status']) {
                DB::commit();

                if ($order['data']['status'] == 1) WhatsappService::order($order['data']['order_id']);
                flash('Success create ' . $this->module)->success();
                return redirect()->route($this->module . '.index');
            } else {
                DB::rollBack();
                flash($order['message'])->error();
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->find($id);
        $data['detail'] = $get;
        $data['shows'] = [
            'reference_code',
            'type',
            'reference_id',
            'sku',
            'price',
            'qty',
            'total',
            'created_at',
        ];
        return view('pages.' . $this->module . '.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $get->password = "";
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);
        $data['detail'] = $get;

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderProductRequest $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        try {
            $input = $request->all();
            $post = $this->repository->find($id);
            $post->update($input);
            gilog("Create " . $this->module, $post, $input);
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
    }
}
