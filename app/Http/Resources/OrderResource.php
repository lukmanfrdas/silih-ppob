<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'reference_code' => $this->reference_code,
            'type' => $this->type,
            'product_name' => data_get($this,'product.name'),
            'number' => $this->number,
            'sku' => $this->sku,
            'status' => $this->status,
            'price' => $this->price,
            'qty' => $this->qty,
            'total' => $this->total,
            'agent_name' => data_get($this,'agent.name'),
            'unit' => $this->unit,
            'created_at' => $this->created_at,
        ];
    }
}
