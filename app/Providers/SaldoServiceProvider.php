<?php

namespace App\Providers;

use App\Services\BalanceService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Svakode\Svaflazz\Svaflazz;

class SaldoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            ['layouts.*'],
            function ($view) {
                if (Auth::user()->type == 'super-admin') {
                    $balance = Svaflazz::checkBalance();
                    $balance = data_get($balance, 'data.deposit', 0);
                } else {
                    $balance = BalanceService::getBalance();
                }

                $view->with([
                    'balance' => (float) $balance,
                ]);
            }
        );
    }
}
