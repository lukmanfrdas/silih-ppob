<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\WhatsappService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BookingReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $month_last = date('t', strtotime('-1 month'));
            $date = date('d');
            $day = 1;
            if ($date != $month_last) $day = $date + 1;

            $users = User::whereDepositDay($day)->pluck('deposit_amount', 'phone');
            foreach ($users as $key => $value) {
                $text = '*SILIH PENGINGAT* Jangan lupa besok untuk deposit ke Silih sebesar ' . number_format($value) . '. Terimakasih';
                WhatsappService::do($key, $text);
            }

            $data['message'] = 'Successfuly, ' . count($users) . ' Deposit Reminder';
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }

        Log::info('deposit:reminder', ['status' => $status, 'data' => $data]);
    }
}
