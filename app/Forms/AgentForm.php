<?php

namespace App\Forms;

use App\Models\User;
use Kris\LaravelFormBuilder\Form;

class AgentForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', ['attr' => ['class' => 'form-control']])
            ->add('email', 'text', ['attr' => ['class' => 'form-control']])
            ->add('phone', 'text', ['attr' => ['class' => 'form-control']])
            ->add('address', 'text', ['attr' => ['class' => 'form-control']])
            ->add('password', 'password', ['value' => false, 'attr' => ['class' => 'form-control']])
            ->add('agent_approval', 'choice', [
                'choices' => $this->getAgentApproval(),
                'label' => 'Approval Admin',
                'empty_value' => 'Choose Approval Admin',
                'attr' => ['class' => 'form-control select2']
            ])
            ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getAgentApproval()
    {
        return config('status.agent_approval');
    }
}
