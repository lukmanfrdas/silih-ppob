<?php

namespace App\Forms;

use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class BookingForm extends Form
{
    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function buildForm()
    {
        if (in_array($this->user->type, ['super-admin', 'agent'])) {
            $this->add('user_id', 'choice', [
                'choices' => $this->getUsers(),
                'empty_value' => 'Choose User',
                'label' => 'User', 'attr' => ['class' => 'form-control select2']
            ]);
        }

        $this
            ->add('product_id', 'choice', [
                'choices' => $this->getProducts(),
                'empty_value' => 'Choose Product',
                'label' => 'Product', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('number', 'number')
            ->add('qty', 'number')
            ->add('type', 'choice', [
                'choices' => $this->getBookingType(),
                'label' => 'Booking Type',
                'empty_value' => 'Choose Booking Type',
                'attr' => ['class' => 'form-control select2']
            ])
            ->add('day', 'number')
            ->add('status', 'choice', [
                'choices' => $this->getStatus(),
                'label' => 'Status',
                'empty_value' => 'Choose Status',
                'attr' => ['class' => 'form-control select2']
            ])
            ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getBookingType()
    {
        return config('status.booking_type');
    }

    public function getProducts()
    {
        $products = Product::get();

        $data = [];
        foreach ($products as $product) $data[$product->id] = $product->name . ' - ' . number_format($product->price);

        return $data;
    }

    public function getUsers($type = 'user')
    {
        $users = User::where('type', $type);
        if ($this->user->type == 'agent') $users = $users->whereAgentId(Auth::user()->id);
        $users = $users->get();

        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;

        return $data;
    }


    public function getStatus()
    {
        return config('status.active');
    }
}
