<?php

namespace App\Forms;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class TopupSaldoForm extends Form
{
    public function buildForm()
    {
        if (Auth::user()->type == 'super-admin') {
            $this->add('agent_id', 'choice', [
                'choices' => $this->getAgents(),
                'empty_value' => 'Pilih Agent',
                'label' => 'Agent', 'attr' => ['class' => 'form-control select2']
            ]);
        }

        $this
            ->add('amount', 'text', ['label' => 'Besaran'])
            ->add('notes', 'textarea', ['label' => 'Catatan']);
    }

    public function getAgents()
    {
        $users = User::whereType('agent')->get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }
}
