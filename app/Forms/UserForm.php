<?php

namespace App\Forms;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function buildForm()
    {
        $this
            ->add('name', 'text', ['attr' => ['class' => 'form-control']])
            ->add('email', 'text', ['attr' => ['class' => 'form-control']])
            ->add('phone', 'text', ['attr' => ['class' => 'form-control']])
            ->add('address', 'text', ['attr' => ['class' => 'form-control']])
            ->add('password', 'password', ['value' => false, 'attr' => ['class' => 'form-control']])
            ->add('deposit_day', 'number', ['label' => 'Deposit Day', 'attr' => ['class' => 'form-control']])
            ->add('deposit_amount', 'number', ['label' => 'Deposit Amount', 'attr' => ['class' => 'form-control']]);

        if (in_array($this->user->type, ['super-admin'])) {
            $this
                ->add('type', 'choice', [
                    'choices' => $this->getUserType(),
                    'label' => 'User Type',
                    'empty_value' => 'Choose User Type',
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('agent_id', 'choice', [
                    'choices' => $this->getAgents(),
                    'empty_value' => 'Pilih Agent',
                    'label' => 'Agent', 'attr' => ['class' => 'form-control select2']
                ]);
        }

        $this->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getUserType()
    {
        return config('status.type');
    }

    public function getAgents()
    {
        $users = User::where('type', 'agent')->get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }

    public function getUsers()
    {
        $users = User::query();
        $auth = Auth::user();

        if ($auth->type == 'agent') $users = $users->whereAgentId($auth->id);
        $users = $users->get();

        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;

        return $data;
    }
}
