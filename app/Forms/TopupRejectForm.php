<?php

namespace App\Forms;

use App\Models\User;
use Kris\LaravelFormBuilder\Form;

class TopupRejectForm extends Form
{
    public function buildForm()
    {
        $this->add('notes', 'textarea', ['label' => 'Catatan']);
    }
}
