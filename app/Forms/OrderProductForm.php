<?php

namespace App\Forms;

use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class OrderProductForm extends Form
{
    public function buildForm()
    {
        if (Auth::user()->type == 'super-admin') {
            $this->add('agent_id', 'choice', [
                    'choices' => $this->getAgents(),
                    'empty_value' => 'Pilih Agent',
                    'label' => 'Agent', 'attr' => ['class' => 'form-control select2']
                ]);
        }

        $this
            ->add('user_id', 'choice', [
                'choices' => $this->getUsers(),
                'empty_value' => 'Choose User',
                'label' => 'User', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('product_id', 'choice', [
                'choices' => $this->getProducts(),
                'empty_value' => 'Choose Product',
                'label' => 'Product', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('qty', 'text')
            ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getProducts()
    {
        $products = Product::query()
            ->whereHas('category', function ($query) {
                $query->whereName('Kebutuhan Primer');
            })
            ->get();

        $data = [];
        foreach ($products as $product) $data[$product->id] = $product->name . ' - ' . number_format($product->price);

        return $data;
    }

    public function getUsers()
    {
        $users = User::whereIn('type', ['agent', 'user'])->orderBy('name', 'ASC')->get();

        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name . ' - ' . $user->address;

        return $data;
    }

    public function getAgents()
    {
        $users = User::whereType('agent')->get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }
}
