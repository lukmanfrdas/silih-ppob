<?php

namespace App\Forms;

use App\Models\Category;
use Kris\LaravelFormBuilder\Form;

class ProductForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('category_id', 'choice', [
                'choices' => $this->getCategories(),
                'empty_value' => 'Choose Category',
                'label' => 'Category', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('name', 'text')
            ->add('price', 'number')
            ->add('profit', 'number')
            ->add('margin_agent', 'number')
            ->add('sku', 'text')
            ->add('description', 'text')
            ->add('status', 'choice', [
                'choices' => [
                    1 => 'Aktif',
                    0 => 'Tidak Aktif'
                ],
                'empty_value' => 'Choose Status',
                'label' => 'Status', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getCategories()
    {
        $categories = Category::whereStatus(1)->whereNotNull('parent_id')->orderBy('parent_id', 'ASC')->get();
        $data = [];
        foreach ($categories as $category) {
            $data[$category->id] = data_get($category, 'parent.name') . ' - ' . $category->name;
        }
        return $data;
    }
}
