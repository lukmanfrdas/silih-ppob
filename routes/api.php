<?php

use App\Http\Controllers\Api\BalanceController;
use App\Http\Controllers\Api\BookingController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\SvaflazzNotificationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('webhook', [SvaflazzNotificationController::class, 'checkNotification']);
Route::post('login', [LoginController::class, 'login']);

Route::middleware('auth:api')->group(function () {
    Route::get('balance', [BalanceController::class, 'index']);
    Route::get('profile', [ProfileController::class, 'index']);
    Route::put('profile/update', [ProfileController::class, 'update']);

    Route::get('products', [ProductController::class, 'index']);
    Route::get('orders', [OrderController::class, 'index']);
    Route::get('bookings', [BookingController::class, 'index']);
    Route::post('booking/store', [BookingController::class, 'store']);
    Route::delete('booking/{id}/delete', [BookingController::class, 'destroy']);

});
