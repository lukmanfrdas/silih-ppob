<?php

use App\Http\Controllers\AgentController;
use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DepositScheduleController;
use App\Http\Controllers\GeneratepdfController;
use App\Http\Controllers\OrderPascaBayarController;
use App\Http\Controllers\OrderPraBayarController;
use App\Http\Controllers\OrderProductController;
use App\Http\Controllers\ProductController;
use App\Models\Balance;
use App\Models\User;
use App\Services\WhatsappService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('auth/google', [GoogleController::class, 'redirectToGoogle'])->name('auth.google');
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('', [HomeController::class, 'index'])->name('home');
    Route::get('report', [HomeController::class, 'report'])->name('home.report');
    Route::patch('user/login-as/{id}', [UserController::class, 'loginAs'])->name('user.login-as');
    Route::post('user/topup-credit/{id}', [UserController::class, 'topupCredit'])->name('user.topup-credit');
    Route::post('user/topup-saldo/{id}', [UserController::class, 'topupSaldo'])->name('user.topup-saldo');
    Route::post('user/cut-credit/{id}', [UserController::class, 'cutCredit'])->name('user.cut-credit');
    Route::post('user/cut-saldo/{id}', [UserController::class, 'cutSaldo'])->name('user.cut-saldo');
    Route::resource('user', UserController::class);
    Route::resource('permission', PermissionController::class);
    Route::resource('role', RoleController::class);
    Route::post('balance/confirm/{id}', [BalanceController::class, 'confirm'])->name('balance.confirm');
    Route::post('balance/reject/{id}', [BalanceController::class, 'reject'])->name('balance.reject');
    Route::resource('balance', BalanceController::class);
    Route::resource('category', CategoryController::class);

    Route::get('product/{id}/chart', [ProductController::class, 'chart'])->name('product.chart');
    Route::resource('product', ProductController::class);
    Route::patch('agent/login-as/{id}', [AgentController::class, 'loginAs'])->name('agent.login-as');
    Route::resource('agent', AgentController::class);

    Route::resource('order-pra-bayar', OrderPraBayarController::class);
    Route::resource('order-pasca-bayar', OrderPascaBayarController::class);
    Route::resource('order-product', OrderProductController::class);

    Route::get('booking/generate/{id}', [BookingController::class, 'generate'])->name('booking.generate');
    Route::resource('booking', BookingController::class);
    Route::get('user/generate-pdf/{id}', [GeneratepdfController::class, 'generatePDF'])->name('order-product.generate-pdf');

    Route::resource('deposit-schedule', DepositScheduleController::class);
});

Route::get('check-time', function () {
    return date('Y-m-d H:i:s');
});
