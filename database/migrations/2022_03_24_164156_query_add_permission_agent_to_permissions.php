<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class QueryAddPermissionAgentToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = [
            'agent.view',
            'agent.create',
            'agent.update',
            'agent.delete',
        ];
        foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

        $role_admin = Role::whereName('super-admin')->first();
        $role_admin->givePermissionTo($permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = [
            'agent.view',
            'agent.create',
            'agent.update',
            'agent.delete',
        ];
        foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

        $role_admin = Role::whereName('super-admin')->first();
        $role_admin->revokePermissionTo($permissions);
    }
}
