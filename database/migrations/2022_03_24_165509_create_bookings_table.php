<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('user_id');
            $table->uuid('agent_id');
            $table->uuid('product_id');
            $table->string('number')->nullable();
            $table->integer('qty');
            $table->string('type')->comment('weekly, monthly');
            $table->string('day');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });

        $permissions = [
            'booking.view',
            'booking.create',
            'booking.update',
            'booking.delete',
            'booking.generate',
        ];
        foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

        $role_super_admin = Role::whereName('super-admin')->first();
        $role_super_admin->givePermissionTo($permissions);

        $role_agent = Role::whereName('agent')->first();
        $role_agent->givePermissionTo($permissions);

        $role_user = Role::whereName('user')->first();
        $role_user->givePermissionTo($permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');

        $permissions = [
            'booking.view',
            'booking.create',
            'booking.update',
            'booking.delete',
            'booking.generate',
        ];
        foreach ($permissions as $permission) Permission::whereName($permission)->delete();

        $role_super_admin = Role::whereName('super-admin')->first();
        $role_super_admin->revokePermissionTo($permissions);

        $role_agent = Role::whereName('agent')->first();
        $role_agent->revokePermissionTo($permissions);

        $role_user = Role::whereName('user')->first();
        $role_user->revokePermissionTo($permissions);
    }
}
