<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id')->index();
            $table->string('reference_code');
            $table->string('type')->nullable();
            $table->uuid('product_id')->index();
            $table->string('reference_id');
            $table->string('sku')->nullable();
            $table->string('status');
            $table->integer('price')->default(0);
            $table->integer('base_price')->default(0);
            $table->integer('margin_agent')->default(0);
            $table->uuid('agent_id')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
