<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class QueryAddPermissionBalanceToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $balance_permissions = [
            'balance.view',
        ];
        $role_user = Role::whereName('user')->first();
        $role_user->givePermissionTo($balance_permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $balance_permissions = [
            'balance.view',
        ];
        $role_user = Role::whereName('user')->first();
        $role_user->revokePermissionTo($balance_permissions);
    }
}
