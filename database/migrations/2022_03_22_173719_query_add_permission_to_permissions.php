<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class QueryAddPermissionToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = [
            'order-pra-bayar.view',
            'order-pra-bayar.create',
            'order-pra-bayar.update',
            'order-pra-bayar.delete',
            'order-pasca-bayar.view',
            'order-pasca-bayar.create',
            'order-pasca-bayar.update',
            'order-pasca-bayar.delete',
            'order-product.view',
            'order-product.create',
            'order-product.update',
            'order-product.delete',
        ];
        foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

        $role_admin = Role::whereName('super-admin')->first();
        $role_admin->givePermissionTo($permissions);

        $role_agent = Role::whereName('agent')->first();
        $role_agent->givePermissionTo($permissions);

        $user_permissions = [
            'order-pra-bayar.view',
            'order-pasca-bayar.view',
            'order-product.view',
        ];
        $role_user = Role::whereName('user')->first();
        $role_user->givePermissionTo($user_permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = [
            'order-pra-bayar.view',
            'order-pra-bayar.create',
            'order-pra-bayar.update',
            'order-pra-bayar.delete',
            'order-pasca-bayar.view',
            'order-pasca-bayar.create',
            'order-pasca-bayar.update',
            'order-pasca-bayar.delete',
            'order-product.view',
            'order-product.create',
            'order-product.update',
            'order-product.delete',
        ];
        foreach ($permissions as $permission) Permission::whereName($permission)->delete();

        $role_admin = Role::whereName('super-admin')->first();
        $role_admin->revokePermissionTo($permissions);

        $role_agent = Role::whereName('agent')->first();
        $role_agent->revokePermissionTo($permissions);

        $user_permissions = [
            'order-pra-bayar.view',
            'order-pasca-bayar.view',
            'order-product.view',
        ];
        $role_user = Role::whereName('user')->first();
        $role_user->revokePermissionTo($user_permissions);
    }
}
