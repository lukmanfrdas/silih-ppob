<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IndexesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->index('reference_code');
            $table->index('type');
        });

        Schema::table('balances', function (Blueprint $table) {
            $table->index('type');
            $table->index('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropIndex(['reference_code', 'type']);
        });

        Schema::table('balances', function (Blueprint $table) {
            $table->dropIndex(['type', 'category']);
        });
    }
}
