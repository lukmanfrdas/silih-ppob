<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balances', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->string('amount');
            $table->string('saldo');
            $table->string('saldo_before');
            $table->string('reference')->nullable();
            $table->string('notes')->nullable();
            $table->text('data')->nullable();
            $table->string('transaction_date');
            $table->string('status')->nullable();
            $table->uuid('user_id')->index();
            $table->string('category');
            $table->boolean('is_topup');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balances');
    }
}
