<?php

return [
    'type' => [
        'super-admin' => 'Super Admin',
        'agent' => 'Agent',
        'user' => 'User',
    ],
    'balance' => [
        0 => 'Pending',
        1 => 'Success',
        2 => 'Waiting Confirmation',
        99 => 'Reject',
        999 => 'Failed'
    ],
    'order' => [
        0 => 'Pending',
        1 => 'Sukses',
        99 => 'Gagal',
    ],
    'order_history' => [
        0 => 'Pending',
        1 => 'Sukses',
        99 => 'Gagal',
    ],
    'booking_type' => [
        1 => 'weekly',
        2 => 'monthly',
    ],
    'active' => [
        1 => 'Aktif',
        0 => 'Tidak Aktif',
    ],
    'agent_approval' => [
        1 => 'Ya', // Admin approval is required
        0 => 'Tidak',
    ],
];
